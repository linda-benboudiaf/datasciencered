import numpy as np
import pandas as pd

def warn(*args, **kwargs): pass
import warnings
warnings.warn = warn

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score, log_loss

from sklearn.svm import SVC
from sklearn import decomposition

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit

train = pd.read_csv('/home/lbenboudiaf/Bureau/MiniProjet-DataScience/DataSets/DATA1_churn_analysis.csv')
test = pd.read_csv('/home/lbenboudiaf/Bureau/MiniProjet-DataScience/DataSets/test.csv')

def encode(train, test):
    le = LabelEncoder().fit(train.aug_user_type)
    labels = le.transform(train.aug_user_type)           # encode species strings
    classes = list(le.classes_)                    # save column names for submission
    test_ids = test.id                             # save test ids for submission

    train = train.drop(['species', 'id'], axis=1)
    test = test.drop(['id'], axis=1)

    return train, labels, test, test_ids, classes

train, labels, test, test_ids, classes = encode(train, test)
train.head(1)
