# Authors Linda Benboudiaf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import  confusion_matrix, f1_score, accuracy_score
# We need to import Preprocessing to encode string data.
from sklearn.preprocessing import LabelEncoder

datasets = pd.read_csv('/home/lbenboudiaf/Bureau/MiniProjet-DataScience/DataSets/DATA2_churn_analysis.csv')
# Data is already shuffled.

#print(le.fit_transform(datasets.aug_user_type.factorize()[0]))
#datasets.print(labels=['aug_user_type', 'sep_user_type'])
# Split Data

lb_make = LabelEncoder()
datasets['international plan']= lb_make.fit_transform(datasets['international plan'].factorize()[0])
datasets['voice mail plan']= lb_make.fit_transform(datasets['voice mail plan'].factorize()[0])
#datasets['state']= lb_make.fit_transform(datasets['state'].factorize()[0])
#datasets['phone number']= lb_make.fit_transform(datasets['phone number'].factorize()[0])
datasets.drop(['state','phone number'], axis= 1, inplace=True)

#Label
datasets['churn']= lb_make.fit_transform(datasets['churn'].factorize()[0])

X = datasets.iloc[:,0:19] #Data, don't worry it doesn't include the last colunm.
y = datasets.churn #Target

X_train, X_test, y_train, y_test = train_test_split(X, y,test_size = 0.20) # We take 80% for trainning set and 20% for Test set.


#Encode the last Colomn for example give 0 to Churned and 1 ot Active
#Creating Label encoder.
# Feature Scaling -> Caractersitique scalaire
sc = StandardScaler()
sc.fit(X_train)
X_train = sc.transform(X_train.astype(float))
X_test = sc.transform(X_test.astype(float))

# in order to determine the best value to 'K'
# in this case we have 2 classes so it is better ti have an odd number like 3, 7, 11 ...
import math
print('Value for K Math.sqrt(len of X_train) -------> ',math.sqrt(len(X_train))) # it gives 30.34 so we take 29 as first best value to 'K'
#Define the Model: K-NN
# p=2 because we want to identifie weather the user is a churned or not.
# We take the euclidean distance between a given data point and the actual data point.
# EuclideanDistance = srqt(pow(x-xi,2) + pw(y-yi,2));
#classifier = KNeighborsClassifier(n_neighbors= 3, p=2,metric= 'euclidean', weights='distance')
#classifier.fit(X_train, y_train)

# Predict the test set results
#y_pred = classifier.predict(X_test)

print("Please wait for graph representation ....")

accuracy = [] #We agregate the Accuracy averages for 18 neighbors.
f1_scores = [] #Metrics...
index = range(3,50)
for i in index:
    classifier = KNeighborsClassifier(n_neighbors = i,metric= 'euclidean', weights='uniform', leaf_size= 30) #27 classifiers
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test) # Predict the class labels for the provided data
    conf_matrix = confusion_matrix(y_test, y_pred) # What we predit <VS> what actually is on test data.
    res = (conf_matrix[0, 0] + conf_matrix[1, 1]) / sum(sum(conf_matrix)) # Calculate Accuracy of our predit.
    accuracy.append(res)
    f1_scores.append(list(zip(y_test, y_pred)))

print('In the range of 3 to 39 we have this values of accuracy')
print(accuracy)

# Evaluate the Model.
print('We evaluate the Matrix of Confusion')
mc = confusion_matrix(y_test, y_pred)
print(mc)

# Graph representation

''''https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.figure.html'''
plt.figure(figsize=(10, 6), num='Knn Algorithm on Churn Analysis Dataset 2')
plt.plot(index, accuracy, color='red', linestyle='dashed', marker='o',
         markerfacecolor='yellow', markersize=10)
plt.title('Accuracy ratio according to K values')
plt.xlabel('K Values')
plt.ylabel('Accuracy average')
plt.show()

#print(f1_score(y_test, y_pred))
#print(accuracy_score(y_test, y_pred))
