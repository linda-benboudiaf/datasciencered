import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

train = pd.read_csv('/home/lbenboudiaf/Bureau/MiniProjet-DataScience/DataSets/DATA1_churn_analysis.csv')

#train['international plan'] = pd.get_dummies(train['international plan'], drop_first=True)
#train['voice mail plan'] = pd.get_dummies(train['voice mail plan'], drop_first=True)
train['Class'] = pd.get_dummies(train['Class'], drop_first=True)
train['sep_fav_a'] = pd.get_dummies(train['sep_fav_a'], drop_first=True)


train.drop(['network_age','Aggregate_complaint_count', 'Aggregate_ONNET_REV','Aggregate_Calls','Aggregate_Data_Vol','Aggregate_Data_Rev','aug_fav_a','sep_user_type','aug_user_type'], axis= 1, inplace=True)
print(np.shape(train))

X = train.iloc[:,0:4] #Data, don't worry it doesn't include the last colunm.
y = train.iloc[:,4]

# Import module to split dataset
from sklearn.model_selection import train_test_split
# Split data set into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

# Import module for fitting
from sklearn.linear_model import LogisticRegression
# Create instance (i.e. object) of LogisticRegression
logmodel = LogisticRegression()
logmodel.fit(X_train, y_train)
y_predp = logmodel.predict(X_test)
from sklearn.metrics import classification_report, confusion_matrix
print(classification_report(y_test, y_predp))
print(confusion_matrix(y_test, y_predp))

# filter out the applicants that got churn
churned = train.loc[y == 1]
# filter out the applicants that din't get churned
not_churned = train.loc[y == 0]

plt.scatter(churned.iloc[:, 0], churned.iloc[:, 1], s=5, label='churned')
plt.scatter(not_churned.iloc[:, 0], not_churned.iloc[:, 1], s=5, label='not_churned')
plt.legend()
plt.show()
