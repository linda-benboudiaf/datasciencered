import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import classification_report, confusion_matrix

lb_make = LabelEncoder()
# we create 40 separable points
#Get the data.
datasets = pd.read_csv('/home/lbenboudiaf/Bureau/MiniProjet-DataScience/DataSets/DATA1_churn_analysis.csv')

datasets['aug_user_type']= lb_make.fit_transform(datasets['aug_user_type'].factorize()[0])
datasets['sep_user_type']= lb_make.fit_transform(datasets['sep_user_type'].factorize()[0])
datasets['aug_fav_a']= lb_make.fit_transform(datasets['aug_fav_a'].factorize()[0])
datasets['sep_fav_a']= lb_make.fit_transform(datasets['sep_fav_a'].factorize()[0])
#datasets.drop(['network_age','Aggregate_complaint_count', 'Aggregate_ONNET_REV','Aggregate_Calls','Aggregate_Data_Vol','Aggregate_Data_Rev','aug_fav_a','sep_user_type','aug_user_type'], axis= 1, inplace=True)


X = datasets.drop('Class', axis=1)
y = lb_make.fit_transform(datasets['Class'].factorize()[0])

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20)

print('--------------------------- Wait for Classification report and Confusion Matrix --------------------------')
print('--------------------------- Implementing Kernel SVM | Linear --------------------------')

svclassifier1 = svm.SVC(kernel='linear')
print(svclassifier1)
svclassifier1.fit(X_train, y_train)
print(svclassifier1.score(X_test, y_test))
#Make predict
y_pred = svclassifier1.predict(X_test)
#Evaluating the Algorithm
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))
exit()
print('------------------------ Implementing Kernel SVM | Sigmoid ------------------------')
svclassifier4 = svm.SVC(kernel='sigmoid')
svclassifier4.fit(X_train, y_train)
#making predict
y_preds = svclassifier4.predict(X_test)
#Evaluating Algorithm
print(confusion_matrix(y_test, y_preds))
print(classification_report(y_test, y_preds))
exit()
print('------------------------ Implementing Kernel SVM | Gaussian ------------------------')
svclassifier3 = svm.SVC(kernel='rbf', C=10)
svclassifier3.fit(X_train, y_train)
#making predit
y_predg = svclassifier3.predict(X_test)
#Evaluating Algorithm
print(confusion_matrix(y_test, y_predg))
print(classification_report(y_test, y_predg))
exit()


print('------------------------ Implementing Kernel SVM | Polynomial  ------------------------')
svclassifier2 = svm.SVC(kernel='poly', coef0=0.5, degree=3) # this is the degre of the polynomial.
svclassifier2.fit(X_train, y_train)
#making prediction
y_predp = svclassifier2.predict(X_test)
#evaluating the poly svm
print(confusion_matrix(y_test, y_predp))
print(classification_report(y_test, y_predp))
exit()
