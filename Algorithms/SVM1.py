import numpy as np
from numpy import *
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import classification_report, confusion_matrix

lb_make = LabelEncoder()
# we create 40 separable points
#Get the data.
datasets = pd.read_csv('/home/lbenboudiaf/Bureau/MiniProjet-DataScience/DataSets/DATA2_churn_analysis.csv')

datasets['international plan']= lb_make.fit_transform(datasets['international plan'].factorize()[0])
datasets['voice mail plan']= lb_make.fit_transform(datasets['voice mail plan'].factorize()[0])
datasets['churn']= lb_make.fit_transform(datasets['churn'].factorize()[0])
#datasets.drop(['state','number vmail messages', 'phone number','account length', 'customer service calls', 'area code', 'voice mail plan','international plan' ], axis= 1, inplace=True)
datasets.drop(['phone number', 'state'], axis= 1, inplace=True)


X = datasets.drop('churn', axis=1)
y = datasets.churn

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.40)

#plt.scatter(X, X, s=30, c=, alpha=0.5)
#plt.scatter(X[:, 0], X[:, 1], c=y, s=30, cmap=plt.cm.Paired)
#exit()

print('--------------------------- Wait for Classification report and Confusion Matrix --------------------------')
print('------------------------ Implementing Kernel SVM | Polynomial  ------------------------')
svclassifier2 = svm.SVC(kernel='poly', degree=8,C=150) # this is the degre of the polynomial.
svclassifier2.fit(X_train, y_train)
#making prediction
y_predp = svclassifier2.predict(X_test)
#evaluating the poly svm
print(confusion_matrix(y_test, y_predp))
print(classification_report(y_test, y_predp))
exit()
print('--------------------------- Implementing Kernel SVM | Linear --------------------------')

svclassifier1 = svm.SVC(kernel='linear')
svclassifier1.fit(X_train, y_train)
#Make predict
y_pred = svclassifier1.predict(X_test)
#Evaluating the Algorithm
print(svclassifier1.score(X_test, y_test))
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))
exit()
print('------------------------ Implementing Kernel SVM | Sigmoid ------------------------')
svclassifier4 = svm.SVC(kernel='sigmoid')
svclassifier4.fit(X_train, y_train)
#making predict
y_preds = svclassifier4.predict(X_test)
#Evaluating Algorithm
print(confusion_matrix(y_test, y_preds))
print(classification_report(y_test, y_preds))
exit()

print('------------------------ Implementing Kernel SVM | Gaussian ------------------------')
svclassifier3 = svm.SVC(kernel='rbf')
svclassifier3.fit(X_train, y_train)
#making predit
y_predg = svclassifier3.predict(X_test)
#Evaluating Algorithm
print(confusion_matrix(y_test, y_predg))
print(classification_report(y_test, y_predg))
exit()







#plt.scatter(X[:, 0], X[:, 1], c=y, s=30, cmap=plt.cm.Paired)
exit()

# plot the decision function
ax = plt.gca()
xlim = ax.get_xlim()
ylim = ax.get_ylim()

# create grid to evaluate model
xx = np.linspace(xlim[0], xlim[1], 30)
yy = np.linspace(ylim[0], ylim[1], 30)
YY, XX = np.meshgrid(yy, xx)
xy = np.vstack([XX.ravel(), YY.ravel()]).T
Z = clf.decision_function(xy).reshape(XX.shape)

# plot decision boundary and margins
ax.contour(XX, YY, Z, colors='k', levels=[-1, 0, 1], alpha=0.5,
           linestyles=['--', '-', '--'])
# plot support vectors
ax.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1], s=100,
           linewidth=1, facecolors='none', edgecolors='k')
plt.show()
