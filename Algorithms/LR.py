# Import data analysis modules
import numpy as np
import pandas as pd

# Use pandas to read in csv file
train = pd.read_csv('/home/lbenboudiaf/Bureau/MiniProjet-DataScience/DataSets/DATA1_churn_analysis.csv')
train2 = pd.read_csv('/home/lbenboudiaf/Bureau/MiniProjet-DataScience/DataSets/DATA2_churn_analysis.csv')

import matplotlib.pyplot as plt
import seaborn as sns

# Use the .isnull() method to detectz missing data
missing_values = train.isnull()
missing_values2 = train2.isnull()

#sns.heatmap(data = missing_values, yticklabels=False, cbar=False, cmap='viridis')
#sns.heatmap(data = missing_values2, yticklabels=False, cbar=False, cmap='viridis')
 #plt.show()
#exit()
# Use the countplot() method to identify ratio of who churned vs. not
# (Tip) very helpful to get a visualization of the target label
# x -> argument referes to column of interest
# data -> argument refers to dataset
#sns.countplot(x='Class', data=train)
#sns.countplot(x='churn', data=train2)
#plt.show()

# Use the countplot() method to identify ratio of who churned vs. with total day calls
# x -> argument referes to column of interest
# data -> argument refers to dataset
# hue -> allows another level to subdivide data
# palette -> argument refers to plot color


#Identifier the outlier using box plot.
#plt.figure(figsize=(10,7))
#sns.boxplot(x= 'area code', y= 'total day calls', data= train2)


train2['international plan'] = pd.get_dummies(train2['international plan'], drop_first=True)
train2['voice mail plan'] = pd.get_dummies(train2['voice mail plan'], drop_first=True)
train2['churn'] = pd.get_dummies(train2['churn'], drop_first=True)
#train2['state'] = pd.get_dummies(train2['state'], drop_first=True)

# I think we can drop some insignicante colmns.state is not important because we have code
# area which is more accurate.

train2.drop(['state','number vmail messages', 'phone number','account length', 'customer service calls', 'area code', 'voice mail plan','international plan' ], axis= 1, inplace=True)
X = train2.iloc[:,0:12] #Data, don't worry it doesn't include the last colunm.
y = train2.iloc[:,12]

# Import module to split dataset
from sklearn.model_selection import train_test_split
# Split data set into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)

#plt.show()

# Import module for fitting
from sklearn.linear_model import LogisticRegression
# Create instance (i.e. object) of LogisticRegression
logmodel = LogisticRegression()
logmodel.fit(X_train, y_train)
y_predp = logmodel.predict(X_test)
from sklearn.metrics import classification_report, confusion_matrix
print(classification_report(y_test, y_predp))
print(confusion_matrix(y_test, y_predp))

# filter out the applicants that got churn
churned = train2.loc[y == 1]
# filter out the applicants that din't get churned
not_churned = train2.loc[y == 0]

plt.scatter(churned.iloc[:, 0], churned.iloc[:, 1], s=5, label='churned')
plt.scatter(not_churned.iloc[:, 0], not_churned.iloc[:, 1], s=5, label='not_churned')
plt.legend()
plt.show()
#
